<?php

namespace Drupal\physical_quantity_fields\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Render\Markup;

/**
 * Plugin implementation of the 'physical_quantity_day' formatter.
 *
 * @FieldFormatter(
 *   id = "physical_quantity_day_default_formatter",
 *   module = "physical_quantity_day",
 *   label = @Translation("Default Formatter"),
 *   field_types = {
 *     "physical_quantity_day"
 *   }
 * )
 */
class DayDefaultFormatter extends FormatterBase {

  /**
   * Default unit for the formatter.
   *
   * @var string
   */
  protected $defaultUnit = "day";

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'measurement_unit' => 'day',
      'suffix_type' => 'abbreviation',
      'suffix_with_dot' => TRUE,
      'suffix_text_transfrom' => 'ucwords',
      'rounding_precision' => '2',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $element = parent::settingsForm($form, $form_state);
    $physical_quantity_time = physical_quantity_fields_time($get_key = '', $singular = TRUE);

    $element['measurement_unit'] = [
      '#type' => 'select',
      '#title' => $this->t('Unit of Measurement'),
      '#description' => $this->t('Select the Unit of Measurement to convert'),
      '#options' => $physical_quantity_time,
      '#default_value' => $this->getSetting('measurement_unit') ?? $this->defaultUnit,
      '#required' => FALSE,
    ];

    $element['suffix_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Suffix type'),
      '#description' => $this->t('Select the measurement suffix type'),
      '#options' => [
        'none' => $this->t('None(empty)'),
        'unit_name' => $this->t('Name of Unit'),
        'abbreviation' => $this->t('Abbreviation of Unit'),
      ],
      '#default_value' => $this->getSetting('suffix_type') ?? 'abbreviation',
      '#required' => FALSE,
    ];

    $element['suffix_text_transfrom'] = [
      '#type' => 'select',
      '#title' => $this->t('Suffix text transform'),
      '#description' => $this->t('Changing the Text Case'),
      '#options' => [
        'strtolower' => $this->t('All letter small'),
        'strtoupper' => $this->t('All letter capital'),
        'ucwords' => $this->t('First letter capital'),
      ],
      '#default_value' => $this->getSetting('suffix_text_transfrom') ?? 'ucwords',
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][suffix_type]"]' => ['!value' => 'none'],
        ],
      ],
    ];

    $element['suffix_with_dot'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Unit of Measurement with dot'),
      '#description' => $this->t('Put a dot at the end of the Unit of Measurement'),
      '#default_value' => $this->getSetting('suffix_with_dot') ?? TRUE,
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][suffix_type]"]' => ['value' => 'abbreviation'],
        ],
      ],
    ];

    $element['rounding_precision'] = [
      '#type' => 'select',
      '#title' => $this->t('Rounding decimal numbers'),
      '#description' => $this->t('Round decimals to a certain accuracy or number of decimal places. The precision value specifies the number of decimal digits to round to'),
      '#options' => [
        'none' => $this->t('Display number without rounding'),
        '0' => $this->t('0 precision'),
        '1' => $this->t('1 precision'),
        '2' => $this->t('2 precision'),
        '3' => $this->t('3 precision'),
        '4' => $this->t('4 precision'),
      ],
      '#default_value' => $this->getSetting('rounding_precision') ?? '2',
      '#required' => FALSE,
      '#states' => [
        'visible' => [
          ':input[name$="[settings_edit_form][settings][suffix_type]"]' => ['!value' => 'none'],
        ],
      ],
    ];

    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $measurement_unit = $this->getSetting('measurement_unit');
    $suffix_type = $this->getSetting('suffix_type');
    $rounding_precision = $this->getSetting('rounding_precision');

    $summary_content = 'Unit of Measurement: ' . $measurement_unit . '<br>';
    $summary_content .= 'Suffix type: ' . $suffix_type . '<br>';
    $summary_content .= 'Rounding precision: ' . $rounding_precision . '<br>';

    // Implement settings summary.
    $summary[] = Markup::create($summary_content);

    return $summary;
  }

  /**
   * Builds a renderable array for a field value.
   *
   * @param \Drupal\Core\Field\FieldItemListInterface $items
   *   The field values to be rendered.
   * @param string $langcode
   *   The language that should be used to render the field.
   *
   * @return array
   *   A renderable array for $items, as an array of child elements keyed by
   *   consecutive numeric indexes starting from 0.
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = [];
    $measurement_unit = $this->getSetting('measurement_unit');
    $suffix_type = $this->getSetting('suffix_type');
    $rounding_precision = $this->getSetting('rounding_precision');
    $suffix = $measurement_unit;

    foreach ($items as $delta => $item) {

      $quanity_value = $item->physical_quantity_day;

      if ($measurement_unit != $this->defaultUnit) {
        $quanity_value = physical_quantity_fields_day_converter($quanity_value, $measurement_unit);
      }

      // Round the value if set.
      if ($rounding_precision != 'none') {
        $quanity_value = round($quanity_value, $rounding_precision);
      }

      if ($suffix_type != 'none') {
        if ($suffix_type == 'unit_name') {
          $singular = TRUE;
          if ($quanity_value > 1) {
            $singular = FALSE;
          }
          $physical_quantity_days = physical_quantity_fields_time($get_key = $measurement_unit, $singular);
          $suffix = $physical_quantity_days;
        }

        $suffix_text_transfrom = $this->getSetting('suffix_text_transfrom');
        switch ($suffix_text_transfrom) {
          case 'strtolower':
            $suffix = strtolower($suffix);
            break;

          case 'strtoupper':
            $suffix = strtoupper($suffix);
            break;

          default:
            $suffix = ucwords($suffix);
            break;
        }

        // Check if the suffix ends with dot or not
        // add the dot at the end if the suffix does not end with dot.
        if (!str_ends_with($suffix, '.')) {
          $suffix_with_dot = $this->getSetting('suffix_with_dot');
          if ($suffix_with_dot) {
            $suffix = $suffix . '.';
          }
        }
      }
      else {
        $suffix = '';
      }

      $other_data['suffix'] = $suffix;

      $elements[$delta] = [
        '#theme' => 'physical_quantity',
        '#physical_quantity_data' => $quanity_value,
        '#other_data' => $other_data,
      ];
    }

    return $elements;
  }

}
